import xbmc,xbmcaddon,xbmcgui,xbmcplugin,urllib,urllib2,os,re,sys,base64

addon_id            = 'plugin.audio.anfieldindex'
AddonTitle          = '[COLOR red][B]Anfield Index Podcast[/B][/COLOR]'
fanarts             = xbmc.translatePath(os.path.join('special://home/addons/' + addon_id , 'fanart.jpg'))
icon                = xbmc.translatePath(os.path.join('special://home/addons/' + addon_id, 'icon.png'))
dialog              = xbmcgui.Dialog()
dp                  = xbmcgui.DialogProgress()
baseurl             = base64.b64decode(b'aHR0cDovL2FuZmllbGRpbmRleC5jb20vcG9kY2FzdHM=')

def GetMenu():

	result = open_url(baseurl)
	match = re.compile('<main class="container podcasts">(.+?)<main class="container">',re.DOTALL).findall(result)[0]
	match2 = re.compile('<a(.+?)</a>',re.DOTALL).findall(match)
	for item in match2:
		try:
			name = re.compile('title="(.+?)"',re.DOTALL).findall(item)[0]
			url = re.compile('href="(.+?)"',re.DOTALL).findall(item)[0]
			iconimage = re.compile('data-src="(.+?)"',re.DOTALL).findall(item)[0]
			url = "http://anfieldindex.com" + url
			addDir('[COLOR red][B]' + name + '[/B][/COLOR]',url,1,iconimage,fanarts)
		except: pass
	xbmc.executebuiltin('Container.SetViewMode(55)')

def GetPodcasts(name,url,iconimage):

	result = open_url(url)
	match = re.compile('<article class="card podcast">(.+?)</article>',re.DOTALL).findall(result)
	for item in match:
		try:
			name = re.compile('<h3 class="title primary-color">(.+?)</h3>',re.DOTALL).findall(item)[0]
			url = re.compile('<a target="_blank" href="(.+?)">',re.DOTALL).findall(item)[0]
			iconimage = re.compile('data-src="(.+?)"',re.DOTALL).findall(item)[0]
			name = name.replace('&amp;','&')
			addLink('[COLOR red][B]' + name + '[/B][/COLOR]',url,2,iconimage,fanarts)
		except: pass
	xbmc.executebuiltin('Container.SetViewMode(55)')

def PlayLink(name,url,iconimage):

	liz = xbmcgui.ListItem(name, iconImage=iconimage, thumbnailImage=iconimage)
	xbmc.Player().play(url,liz,False)

def open_url(url):

    req = urllib2.Request(url)
    req.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36')
    response = urllib2.urlopen(req)
    link=response.read()
    link=link.replace('\n','').replace('\r','')
    response.close()
    return link

def get_params():
        param=[]
        paramstring=sys.argv[2]
        if len(paramstring)>=2:
                params=sys.argv[2]
                cleanedparams=params.replace('?','')
                if (params[len(params)-1]=='/'):
                        params=params[0:len(params)-2]
                pairsofparams=cleanedparams.split('&')
                param={}
                for i in range(len(pairsofparams)):
                        splitparams={}
                        splitparams=pairsofparams[i].split('=')
                        if (len(splitparams))==2:
                                param[splitparams[0]]=splitparams[1]                    
        return param
	
def addDir(name,url,mode,iconimage,fanart,description=''):

	u=sys.argv[0]+"?url="+urllib.quote_plus(url)+"&mode="+str(mode)+"&name="+urllib.quote_plus(name)+"&iconimage="+urllib.quote_plus(iconimage)+"&fanart="+urllib.quote_plus(fanart)
	ok=True
	liz=xbmcgui.ListItem(name, iconImage=iconimage, thumbnailImage=iconimage)
	liz.setProperty( "fanart_Image", fanart )
	liz.setProperty( "icon_Image", iconimage )
	ok=xbmcplugin.addDirectoryItem(handle=int(sys.argv[1]),url=u,listitem=liz,isFolder=True)
	return ok

def addLink(name, url, mode, iconimage, fanart, description=''):

	u=sys.argv[0]+"?url="+urllib.quote_plus(url)+"&mode="+str(mode)+"&name="+urllib.quote_plus(name)+"&iconimage="+urllib.quote_plus(iconimage)+"&fanart="+urllib.quote_plus(fanart)
	ok=True
	liz=xbmcgui.ListItem(name, iconImage=iconimage, thumbnailImage=iconimage)
	liz.setProperty( "fanart_Image", fanart )
	liz.setProperty( "icon_Image", iconimage )
	ok=xbmcplugin.addDirectoryItem(handle=int(sys.argv[1]),url=u,listitem=liz,isFolder=False)
	return ok

params=get_params(); url=None; name=None; mode=None; site=None; iconimage=None; fanart=None
try: site=urllib.unquote_plus(params["site"])
except: pass
try: url=urllib.unquote_plus(params["url"])
except: pass
try: name=urllib.unquote_plus(params["name"])
except: pass
try: mode=int(params["mode"])
except: pass
try: iconimage=urllib.unquote_plus(params["iconimage"])
except: pass
try: fanart=urllib.unquote_plus(params["fanart"])
except: pass
 
if mode==None or url==None or len(url)<1: GetMenu()
elif mode==1:GetPodcasts(name,url,iconimage)
elif mode==2:PlayLink(name,url,iconimage)

xbmcplugin.endOfDirectory(int(sys.argv[1]))